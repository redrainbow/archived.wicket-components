package de.redrainbow.wicket.search;

import java.util.List;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

public abstract class SearchPanel<T> extends Panel {
    private static final long serialVersionUID = 1L;

    private String query;
    private IModel<String> queryModel;

    public SearchPanel(String id) {
        super(id);
    }

    protected abstract SearchForm createSearchForm(String id, IModel<String> queryModel);
    protected abstract Panel createSearchResulListPanel(String id, IModel<List<T>> resultListModel);

    @Override
    protected void onInitialize() {
	super.onInitialize();
        this.queryModel = new PropertyModel<>(this, "query");
	SearchForm form = createSearchForm("searchform", queryModel);
	add(form);

        TextField queryField = new TextField("searchquery", queryModel);
        queryField.add(new AjaxFormComponentUpdatingBehavior("onkeyup") {
            @Override
            protected void onUpdate(AjaxRequestTarget art) {
                SearchForm form = (SearchForm) SearchPanel.this.get("searchform");
                form.performSearch();
                art.add(form.get("searchresults"));
            }
        });
        form.add(queryField);

        WebMarkupContainer searchresults = new WebMarkupContainer("searchresults");
        searchresults.setOutputMarkupId(true);
        form.add(searchresults);

        searchresults.add(new Label("numResults", new PropertyModel(form, "resultSize")) {
            @Override
            public boolean isVisible() {
                SearchForm form = (SearchForm) SearchPanel.this.get("searchform");
                return query != null && !query.isEmpty();
            }
        });

        SearchDataProvider<T> provider = form.getDataProvider();
        searchresults.add(createSearchResulListPanel("results", provider.getPagedSearchResultsModel()));
        searchresults.add(new SearchResultsPager("pager", form) {
            @Override
            protected void performAjaxUpdates(AjaxRequestTarget target) {
                SearchForm form = (SearchForm) SearchPanel.this.get("searchform");
                target.add(form.get("searchresults"));
            }
        });
    }


    @Override
    protected void onDetach() {
        super.onDetach();
        if (this.queryModel != null)
            this.queryModel.detach();
    }

    public void setSearchQuery(String query) {
        this.query = query;
    }

}
