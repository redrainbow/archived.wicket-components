/*
 * Copyright 2014 Red Rainbow IT Solutions GmbH, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.redrainbow.wicket.search;

import java.io.Serializable;

public class SearchResultsNumberBean implements Serializable {

    private final int shown;
    private final int count;

    public SearchResultsNumberBean(int shown, int count) {
	this.shown = shown;
	this.count = count;
    }

    public int getShown() {
	return shown;
    }

    public int getCount() {
	return count;
    }

    public int getMissing() {
	return count - shown;
    }
    
}
