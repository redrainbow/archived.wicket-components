/*
 * Copyright 2014 Red Rainbow IT Solutions GmbH, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.redrainbow.wicket.search;

import java.util.List;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.navigation.paging.IPageable;
import org.apache.wicket.model.IModel;

public abstract class SearchForm<T> extends Form<String> implements IPageable {

    private SearchDataProvider<T> dataProvider;

    public SearchForm(String id, IModel<String> queryModel) {
	super(id, queryModel);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        this.dataProvider = createDataProvider();
    }

    protected abstract SearchDataProvider<T> createDataProvider();

    @Override
    protected void onSubmit() {
        performSearch();
    }

    public void performSearch() {
        this.dataProvider.performSearch();
    }

    public SearchDataProvider<T> getDataProvider() {
        return this.dataProvider;
    }

    public IModel<String> getSearchQueryModel(){
        return this.dataProvider.getSearchQueryModel();
    }

    public IModel<List<T>> getSearchResultsModel() {
        return this.dataProvider.getPagedSearchResultsModel();
    }

    @Override
    protected void onDetach() {
	super.onDetach();
        dataProvider.detach();
    }

    public long getResultSize() {
        return dataProvider.getResultSize();
    }

    @Override
    public long getPageCount() {
        return dataProvider.getPageCount();
    }

    @Override
    public long getCurrentPage() {
        return dataProvider.getCurrentPage();
    }

    @Override
    public void setCurrentPage(long l) {
        dataProvider.setCurrentPage(l);
    }

}
