/*
 * Copyright 2014 Red Rainbow IT Solutions GmbH, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.redrainbow.wicket.search;

import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.navigation.paging.IPageable;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;

public class SearchResultsPager extends Panel {

    private final IPageable pageable;

    public SearchResultsPager(String id, IPageable pageable) {
        super(id);
        this.pageable = pageable;
    }
    
    @Override
    protected void onInitialize() {
        super.onInitialize();
        this.setOutputMarkupPlaceholderTag(true);

        add(createListItem("firstLi", new FirstPageCommand()));
        add(createListItem("prevLi", new PrevPageCommand()));
        add(createListItem("nextLi", new NextPageCommand()));
        add(createListItem("lastLi", new LastPageCommand()));

        IModel<List<Integer>> pageModel = new AbstractReadOnlyModel<List<Integer>>() {
            @Override
            public List<Integer> getObject() {
                int currentPage = (int) pageable.getCurrentPage();
                int start = Math.max(0, (int) currentPage - 5);
                int end = Math.min((int) pageable.getPageCount() - 1, currentPage + 5);
                List<Integer> res = new ArrayList<>();
                for (int i = start; i <= end; i++)
                    res.add(i);
                return res;
            }
        };
        add(new ListView<Integer>("pages", pageModel) {
            @Override
            protected void populateItem(ListItem<Integer> li) {
                final Integer page = li.getModelObject();
                AbstractLink link = createLink("href", new FixedPageCommand(page.longValue()));
                link.add(new Label("page", "" + (page+1)));
                li.add(link);
                li.add(new AttributeAppender("class", new AbstractReadOnlyModel() {
                    @Override
                    public Object getObject() {
                        return pageable.getCurrentPage() == page.longValue() ? "active" : "";
                    }
                }));
            }
        });
    }

    protected WebMarkupContainer createListItem(String id, final LinkCommand command) {
        WebMarkupContainer cont = new WebMarkupContainer(id);
        cont.add(new AttributeAppender("class", new AbstractReadOnlyModel() {
            @Override
            public Object getObject() {
                return command.isDiabled() ? "disabled" : "";
            }
        }));
        cont.add(createLink("href", command));
        return cont;
    }

    protected AbstractLink createLink(String id, final SearchResultsPagerCommand command) {
        AbstractLink link = new AjaxLink(id) {
            @Override
            public void onClick(AjaxRequestTarget art) {
                command.call();
                performAjaxUpdates(art);
            }
        };
        return link;
    }

    @Override
    public boolean isVisible() {
        return pageable.getPageCount() > 1;
    }

    protected void performAjaxUpdates(AjaxRequestTarget target) { }

    private abstract class LinkCommand implements SearchResultsPagerCommand {
        public void call() {
            long page = linkedPage();
            pageable.setCurrentPage(page);
        }
    }

    protected class FixedPageCommand extends LinkCommand {
        private long page;
        public FixedPageCommand(long page) {
            this.page = page;
        }
        @Override
        public long linkedPage() {
            return page;
        }
        @Override
        public boolean isDiabled() {
            return false;
        }
    }

    private abstract class RelativePageCommand extends LinkCommand {
        @Override
        public boolean isDiabled() {
            long paged = linkedPage();
            long currentPage = pageable.getCurrentPage();
            return paged == currentPage;
        }
    }
    private class FirstPageCommand extends RelativePageCommand {
        @Override
        public long linkedPage() {
            return 0;
        }
    }
    private class PrevPageCommand extends RelativePageCommand {
        @Override
        public long linkedPage() {
            return Math.max(0, pageable.getCurrentPage() - 1);
        }
    }
    private class NextPageCommand extends RelativePageCommand {
        @Override
        public long linkedPage() {
            return Math.min(pageable.getCurrentPage() + 1, pageable.getPageCount() - 1);
        }
    }
    private class LastPageCommand extends RelativePageCommand {
        @Override
        public long linkedPage() {
            return pageable.getPageCount() - 1;
        }
    }

}
