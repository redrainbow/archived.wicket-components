/*
 * Copyright 2014 Red Rainbow IT Solutions GmbH, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.redrainbow.wicket.search;

import java.util.List;
import org.apache.wicket.markup.html.navigation.paging.IPageable;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;

public abstract class SearchDataProvider<T> implements IPageable {

    private final IModel<String> queryModel;
    private IModel<List<T>> searchResultsModel;
    private List<T> searchResults;

    private long currentPage;
    private final long perPage;

    public SearchDataProvider(IModel<String> queryModel, long perPage) {
        this.queryModel = queryModel;
        this.perPage = perPage;
    }

    protected abstract List<T> findResults(String query);

    public final void performSearch() {
        searchResults = findResults(queryModel.getObject());
    }

    public IModel<String> getSearchQueryModel() {
        return queryModel;
    }

    public boolean hasQuery() {
        String s = queryModel.getObject();
        return s != null && !s.isEmpty();
    }

    @Override
    public long getCurrentPage() {
        return currentPage;
    }

    @Override
    public void setCurrentPage(long currentPage) {
        if (currentPage >= getPageCount())
            currentPage = getPageCount() - 1;
        this.currentPage = currentPage;
    }

    public long getResultSize() {
        return getSearchResults().size();
    }

    @Override
    public long getPageCount() {
        long size = getSearchResults().size();
        return (size + perPage - 1)/perPage;
    }

    public IModel<List<T>> getPagedSearchResultsModel() {
        if (searchResultsModel == null) {
            searchResultsModel = new AbstractReadOnlyModel<List<T>>() {
                @Override
                public List<T> getObject() {
                    List<T> results = getSearchResults();
                    long start = currentPage * perPage;
                    long end = Math.min(results.size(), (currentPage + 1) * perPage);
                    return results.subList((int) start, (int) end);
                }
            };
        }
        return searchResultsModel;
    }

    public void detach() {
        searchResultsModel.detach();
        searchResults = null;
    }

    private List<T> getSearchResults() {
        if (searchResults == null)
            performSearch();
        return searchResults;
    }

}
