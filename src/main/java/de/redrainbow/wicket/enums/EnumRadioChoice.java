/*
 * Copyright 2013 Olivier Croisier (thecodersbreakfast.net)
 * Copyright 2013-2014 Red Rainbow IT Solutions GmbH, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.redrainbow.wicket.enums;

import java.util.Arrays;
import java.util.List;
import org.apache.wicket.markup.html.form.EnumChoiceRenderer;
import org.apache.wicket.markup.html.form.RadioChoice;
import org.apache.wicket.model.IModel;

public class EnumRadioChoice<T extends Enum<T>> extends RadioChoice<T> {

    private final Class<? extends T> clazz;

    public EnumRadioChoice(Class<? extends T> clazz, String id) {
        super(id);
        this.clazz = clazz;
        setChoiceRenderer(new EnumChoiceRenderer<T>(this));
    }

    public EnumRadioChoice(Class<? extends T> clazz, String id, IModel<T> model) {
        super(id);
        this.clazz = clazz;
        setModel(model);
        setChoiceRenderer(new EnumChoiceRenderer<T>(this));
    }

    @Override
    public List<? extends T> getChoices() {
        return Arrays.asList(clazz.getEnumConstants());
    }
}