/*
 * Copyright 2013-2014 Red Rainbow IT Solutions GmbH, Germany
 * Copyright 2013 Olivier Croisier (thecodersbreakfast.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.redrainbow.wicket.enums;

import java.util.Arrays;
import java.util.List;
import org.apache.wicket.Component;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Radio;
import org.apache.wicket.markup.html.form.RadioGroup;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.StringResourceModel;

public class EnumRadioGroup<T extends Enum<T>> extends Panel {

    private final Class<? extends T> clazz;
    private final RadioGroup<T> group;

    public EnumRadioGroup(Class<? extends T> clazz, String id, IModel<T> model) {
        super(id);
        this.clazz = clazz;
        this.group = new RadioGroup<>("group", model);
        add(group);
    }

    public List<? extends T> getChoices() {
        return Arrays.asList(clazz.getEnumConstants());
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        IModel<List<? extends T>> model = new AbstractReadOnlyModel<List<? extends T>>() {
            @Override
            public List<? extends T> getObject() {
                return getChoices();
            }
        };

        ListView<T> lv = new ListView<T>("choice", model) {
            @Override
            protected void populateItem(ListItem<T> li) {
                populateListItem(li);
            }
        };
        lv.setReuseItems(true);
        group.add(lv);
    }

    public final Component setRequired(boolean required) {
        group.setRequired(required);
        return this;
    }

    @Override
    protected void onDetach() {
        super.onDetach();
        group.detach();
    }

    protected String resourceKey(T object) {
        return object.getDeclaringClass().getSimpleName() + "." + object.name();
    }

    protected void populateListItem(ListItem<T> li) {
        T val = li.getModelObject();
        String key = resourceKey(val);
        StringResourceModel srm = new StringResourceModel(key, this, li.getModel());
        li.add(new Radio<>("radio", li.getModel()));
        li.add(createListItemLabel("label", srm));
    }

    protected Label createListItemLabel(String id, IModel<String> model) {
        return new Label(id, model);
    }

}