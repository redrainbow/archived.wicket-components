package de.redrainbow.wicket.feedback;

import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.feedback.IFeedbackMessageFilter;

/**
 * From
 * http://jeremythomerson.com/2011/01/04/catching-all-feedback-messages-that-arent-rendered-by-other-feedback-panels/.
 */
public class AllExceptFeedbackFilter implements IFeedbackMessageFilter {

    private static final long serialVersionUID = 1L;

    private IFeedbackMessageFilter[] filters = null;

    public AllExceptFeedbackFilter() { }

    public AllExceptFeedbackFilter(IFeedbackMessageFilter[] filters) {
        this.filters = filters;
    }

    @Override
    public boolean accept(FeedbackMessage message) {
        IFeedbackMessageFilter[] localFilters = getFilters();
        if (localFilters == null)
            return true;
        for (IFeedbackMessageFilter filter : localFilters) {
            if (filter.accept(message)) {
                return false;
            }
        }
        return true;
    }

    protected IFeedbackMessageFilter[] getFilters() {
        return filters;
   }

}
