package de.redrainbow.wicket.feedback;

import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.feedback.IFeedbackMessageFilter;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.util.visit.IVisit;
import org.apache.wicket.util.visit.IVisitor;

/**
 * From
 * http://jeremythomerson.com/2011/01/04/catching-all-feedback-messages-that-arent-rendered-by-other-feedback-panels/.
 */
public class AllUnfilteredMessagesFeedbackFilter extends AllExceptFeedbackFilter {

    private FeedbackPanel myPanel;

    public AllUnfilteredMessagesFeedbackFilter(FeedbackPanel myPanel) {
        this.myPanel = myPanel;
    }

    @Override
    protected IFeedbackMessageFilter[] getFilters() {
      final List<IFeedbackMessageFilter> filters = new ArrayList<>();
      myPanel.getPage().visitChildren(FeedbackPanel.class, new IVisitor<FeedbackPanel, Void>() {
          @Override
          public void component(FeedbackPanel t, IVisit<Void> ivisit) {
            if (myPanel.equals(t)) {
                ivisit.dontGoDeeper();
            } else {
                IFeedbackMessageFilter filter = t.getFilter();
                if (filter != null)
                    filters.add(filter);
            }
          }
      });
      return filters.toArray(new IFeedbackMessageFilter[filters.size()]);
   }

}
