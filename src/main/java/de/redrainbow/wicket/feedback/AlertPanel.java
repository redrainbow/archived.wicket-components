/*
 * This file is derivative work from code found in the Apache Karaf Web
 * Console Project.  The following is the license notice of the original
 * work:
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *************************************************************************
 * 
 * We release our modifications under the Apache License, Version 2.0, too,
 * such that you can use them in your own projects:
 *
 * Copyright 2013-2014 Red Rainbow IT Solutions GmbH, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.redrainbow.wicket.feedback;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;

/**
 * Bootstrap based alert panel.
 * <p>
 * Source: Apache Karaf Web Console
 */
public class AlertPanel extends Panel {

    private static final long serialVersionUID = 1L;

    private WebMarkupContainer wrapper;
    private String message;

    public AlertPanel(String id, String message, AlertType type) {
        this(id, message);
        setType(type);
    }

    public AlertPanel(String id, String message) {
        super(id);
        this.message = message;
        add(wrapper = new WebMarkupContainer("wrapper"));
        Label lab = new Label("message", message);
        wrapper.add(lab);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        Component lab = wrapper.get("message");
        lab.setEscapeModelStrings(getEscapeModelStrings());
    }

    public final void setType(AlertType type) {
        wrapper.add(new AttributeAppender("class", Model.of(type.getCssClass()), " "));
    }

}