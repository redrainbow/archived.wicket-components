/*
 * Copyright 2013-2014 Red Rainbow IT Solutions GmbH, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.redrainbow.wicket.feedback;

import org.apache.wicket.MarkupContainer;
import org.apache.wicket.feedback.ComponentFeedbackMessageFilter;
import org.apache.wicket.feedback.FeedbackMessage;

public class ContainerBootstrapFeedbackPanel extends BootstrapFeedbackPanel {

    public ContainerBootstrapFeedbackPanel(String id, final MarkupContainer comp) {
        super(id, new ComponentFeedbackMessageFilter(comp) {
            @Override
            public boolean accept(FeedbackMessage message) {
                if (message.getReporter() == null) // global messages
                    return false;
                return message.getReporter().equals(comp)
                        || comp.contains(message.getReporter(), true);
            }
        });
    }
    
}